#
# Vivado (TM) v2016.2 (64-bit)
#
# This file contains the Vivado Tcl commands for re-creating the project to the state*
# when this script was generated. In order to re-create the project, please source this
# file in the Vivado Tcl Shell.
#
#*****************************************************************************************

set project_name "Arty"
set part "xc7a35ticsg324-1L"
source "common.tcl"

parse_args

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/$project_name"]"

# Create project
create_project $project_name ./$project_name -part $part

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects $project_name]
set_property "default_lib" "xil_defaultlib" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "Mixed" $obj
set_property "target_language" "VHDL" $obj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY" $obj


# Add sources
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}
#add_vhd_files		sources_1 "xil_defaultlib"	"$origin_dir/sources/example.vhd"

# Add constraints
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -srcset constrs_1
}

add_xdc_files		constrs_1		"$origin_dir/sources/system.xdc"
add_xdc_files		constrs_1		"$origin_dir/sources/pinout.xdc"

# Add simulation files
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -srcset sim_1
}


# Subdiagrams
#source "$origin_dir/sources/HDL/example.tcl"

# Main block diagram
source "$origin_dir/sources/system.tcl"

# Create wrapper for system block diagram
make_wrapper -files [get_files system.bd] -top -import
set_property top system_wrapper [current_fileset]


if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part $part -flow {Vivado Synthesis 2016} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
}
current_run -synthesis [get_runs synth_1]

if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part $part -flow {Vivado Implementation 2016} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
}
set obj [get_runs impl_1]
set_property "steps.write_bitstream.args.bin_file" "1" $obj
set_property "steps.write_bitstream.args.readback_file" "0" $obj
set_property "steps.write_bitstream.args.verbose" "0" $obj
current_run -implementation $obj


# Add ELF bootloader
if {$bootloader_path ne ""} {
	puts "Adding $bootloader_path as ELF file to microblaze"
	add_files -norecurse "$bootloader_path"
	set_property used_in_simulation 0 [get_files "$bootloader_path"]
	set_property SCOPED_TO_REF system [get_files -all -of_objects [get_fileset sources_1] "$bootloader_path"]
	set_property SCOPED_TO_CELLS { microblaze_0 } [get_files -all -of_objects [get_fileset sources_1] "$bootloader_path"]
}

puts "INFO: Project created:$project_name"

if { $build_bitfile == true } {
	puts "INFO: Launching write_bitstream process"
	launch_runs impl_1 -to_step write_bitstream -jobs 4 -verbose
	wait_on_run impl_1
	puts "SYNTHESIS COMPLETED"
	set bitfile [get_property DIRECTORY [current_run]]/system_wrapper.bit
	file copy $bitfile "$project_name.bit"
	write_cfgmem -format mcs -size 128 -interface SPIx4 -checksum -loadbit "up 0x0 $bitfile" -file "$project_name-bitfile.mcs"
}
