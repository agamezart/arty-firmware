# Common functions and initial script

proc add_xdc_files { fileset args } {
  set obj [get_filesets $fileset]
  foreach x $args {
    # Add/Import fileset file and set fileset file properties
    set file "[file normalize "$x"]"
    set file_added [add_files -norecurse -fileset $obj $file]
    set file_obj [get_files -of_objects [get_filesets $fileset] [list "*$file"]]
    set_property "file_type" "XDC" $file_obj
  }
}

proc add_vhd_files { fileset library args } {
  set obj [get_filesets $fileset]
  foreach x $args {
    # Add/Import fileset file and set fileset file properties
    set file "[file normalize "$x"]"
    set file_added [add_files -norecurse -fileset $obj $file]
    set file_obj [get_files -of_objects [get_filesets $fileset] [list "*$file"]]
    set_property "file_type" "VHDL" $file_obj
    set_property "library" $library $file_obj
  }
}

proc add_vhd_sim_files { fileset library args } {
  set obj [get_filesets $fileset]
  foreach x $args {
    # Add/Import fileset file and set fileset file properties
    set file "[file normalize "$x"]"
    set file_added [add_files -norecurse -fileset $obj $file]
    set file_obj [get_files -of_objects [get_filesets $fileset] [list "*$file"]]
    set_property "file_type" "VHDL" $file_obj
    set_property "used_in_synthesis" "0" $file_obj
    set_property "library" $library $file_obj
  }
}

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--build-bitfile\]      Build bitfile after creating project"
  puts "\[--bootloader <path>\]  Select ELF bootloader to embed onto bitfile"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}


proc parse_args {} {
  variable origin_dir
  variable build_bitfile
  variable bootloader_path
  if { $::argc > 0 } {
    for {set i 0} {$i < $::argc} {incr i} {
      set option [string trim [lindex $::argv $i]]
      switch -regexp -- $option {
        "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
        "--build-bitfile" { set build_bitfile true }
        "--bootloader" { incr i; set bootloader_path [lindex $::argv $i] }
        "--help"       { help }
        default {
          if { [regexp {^-} $option] } {
            puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
            return 1
          }
        }
      }
    }
  }
}


# Set default values
set build_bitfile false
set bootloader_path ""


# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir "."

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

variable script_file
set script_file [info script]
