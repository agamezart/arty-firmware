# System

set_property -dict { PACKAGE_PIN C2  IOSTANDARD LVCMOS33 } [get_ports { reset }];
set_property -dict { PACKAGE_PIN E3  IOSTANDARD LVCMOS33 } [get_ports { sys_clock }];

# UART
set_property -dict { PACKAGE_PIN A9  IOSTANDARD LVCMOS33 } [get_ports { uart_rtl_rxd }];
set_property -dict { PACKAGE_PIN D10  IOSTANDARD LVCMOS33 } [get_ports { uart_rtl_txd }];

# ETHERNET
set_property -dict { PACKAGE_PIN G18  IOSTANDARD LVCMOS33 } [get_ports { eth_ref_clk }];

set_property -dict { PACKAGE_PIN D17  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_col }];
set_property -dict { PACKAGE_PIN G14  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_crs }];
set_property -dict { PACKAGE_PIN F16  IOSTANDARD LVCMOS33 } [get_ports { mdio_rtl_mdc }];
set_property -dict { PACKAGE_PIN K13  IOSTANDARD LVCMOS33 } [get_ports { mdio_rtl_mdio_io }];
set_property -dict { PACKAGE_PIN C16  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rst_n }];

set_property -dict { PACKAGE_PIN D18  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rxd[0] }];
set_property -dict { PACKAGE_PIN E17  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rxd[1] }];
set_property -dict { PACKAGE_PIN E18  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rxd[2] }];
set_property -dict { PACKAGE_PIN G17  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rxd[3] }];
set_property -dict { PACKAGE_PIN F15  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rx_clk }];
set_property -dict { PACKAGE_PIN G16  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rx_dv }];
set_property -dict { PACKAGE_PIN C17  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_rx_er }];

set_property -dict { PACKAGE_PIN H14  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_txd[0] }];
set_property -dict { PACKAGE_PIN J14  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_txd[1] }];
set_property -dict { PACKAGE_PIN J13  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_txd[2] }];
set_property -dict { PACKAGE_PIN H17  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_txd[3] }];

set_property -dict { PACKAGE_PIN H16  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_tx_clk }];
set_property -dict { PACKAGE_PIN H15  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_tx_en }];
set_property -dict { PACKAGE_PIN F18  IOSTANDARD LVCMOS33 } [get_ports { mii_rtl_tx_er }];

# FLASH

set_property -dict { PACKAGE_PIN K17  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_io0_io }];
set_property -dict { PACKAGE_PIN K18  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_io1_io }];
set_property -dict { PACKAGE_PIN L14  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_io2_io }];
set_property -dict { PACKAGE_PIN M14  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_io3_io }];
set_property -dict { PACKAGE_PIN L16  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_sck_io }];
set_property -dict { PACKAGE_PIN L13  IOSTANDARD LVCMOS33 } [get_ports { spi_rtl_ss_io }];
